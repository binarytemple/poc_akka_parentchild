import akka.actor.{Cancellable, Props, Actor, ActorSystem}
import akka.routing.RandomRouter
import java.util.concurrent.{TimeUnit, CountDownLatch}
import scala.concurrent.stm._

class WootPut(t: TMap[Int, String]) extends Actor {
  def receive = {
    case (i: Int, b: String) => atomic {
      implicit txn =>
        println(s" ${self.path} trying to update TMap")
        t.put(i, b)
    }
  }
}

class WootShow(t: TMap[Int, String]) extends Actor {
  def receive = {
    case o => atomic {
      implicit txn =>
        println(s" ${self.path} : received $o , displaying  ${t.single}")
    }
  }
}

object STMSampleMain2 {
  def main(args: Array[String]) {
    val as = ActorSystem("db-as")

    val t = TMap[Int, String]()

    val as1 = as.actorOf(Props[WootShow].withCreator(new WootShow(t)))
    import scala.concurrent.duration._
    import scala.concurrent.ExecutionContext.Implicits.global
    val schedule: Cancellable = as.scheduler.schedule((0 seconds), (1 second), as1, 'ping)

    val router1 = as.actorOf(Props[WootPut].withCreator(new WootPut(t)).withRouter(RandomRouter(nrOfInstances = 5)))

    Range(1, 10).map(x => (x , "v-" + x.toString)).foreach(x => { Thread.sleep(1000) ; router1 ! x })
    schedule.cancel()
    println("map was filled")

    val cdl = new CountDownLatch(1)
    cdl.await(5, TimeUnit.SECONDS)

    as.shutdown()
  }
}
