import akka.actor.{Cancellable, Props, Actor, ActorSystem}
import akka.routing.RandomRouter
import java.util.concurrent.{TimeUnit, CountDownLatch}
import scala.concurrent.stm._

class WootPush(t: TSet[Int]) extends Actor {
  def receive = {
    case i: Int => atomic {
      implicit txn =>
        try {
          t += i
          println(s" ${self.path} updated TSet")
        }
        catch {
          case t: Throwable => {println(s"${self.path}: retrying push "); retry}
        }
    }
    case other => System.err.println(s"UNEXPECTED ! $other ")

  }
}

class WootPop(t: TSet[Int]) extends Actor {
  def receive = {
    case 'pop => atomic {
      implicit txn =>
        try {
          println(s"head was ${t.head}")
          t -= t.head
        }
        catch {
          case t: Throwable => {println(s"${self.path}: retrying pop "); retry /* retryFor(1, TimeUnit.SECONDS)*/ }
        }
    }
  }
}

object STMSampleMain3 {
  def main(args: Array[String]) {
    val as = ActorSystem("db-as")
    val t = TSet[Int]()

    import scala.concurrent.duration._
    import scala.concurrent.ExecutionContext.Implicits.global

    val pushRouter = as.actorOf(Props[WootPush].withCreator(new WootPush(t)).withRouter(RandomRouter(nrOfInstances = 5)))
    val popRouter = as.actorOf(Props[WootPush].withCreator(new WootPop(t)).withRouter(RandomRouter(nrOfInstances = 5)))

    val schedule: Cancellable = as.scheduler.schedule((0 seconds), (10 milliseconds), popRouter, 'pop)

    Range(1, 1000).foreach(x => {Thread.sleep(10); pushRouter ! x })
    schedule.cancel()

    val cdl = new CountDownLatch(1)
    cdl.await(15, TimeUnit.SECONDS)

    as.shutdown()
  }
}
