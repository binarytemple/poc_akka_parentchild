import akka.actor._
import akka.actor.SupervisorStrategy._
import java.util.concurrent.atomic.AtomicInteger
import java.util.concurrent.{TimeUnit, CountDownLatch}
import scala.concurrent.duration._

class ResumeMakingException extends RuntimeException
class RestartMakingException extends RuntimeException
class EscalateMakingException extends RuntimeException
class StopMakingException extends RuntimeException

class FooChild extends Actor {
  val counter = new AtomicInteger(0)

  override def postRestart(reason: Throwable) {
    System.err.println(s"RESTARTED DUE TO: $reason"); super.postRestart(reason)
  }

  def receive = {
    case 'explode => throw new ResumeMakingException()
    case 'shoot => throw new RestartMakingException()
    case 'implode => throw new EscalateMakingException()
    case 'armageddon => throw new NullPointerException
    case o => println(s"child + $o   , counter = ${counter.getAndIncrement}")
  }
}

class Foo extends Actor {
  var child = context.actorOf(Props[FooChild])
  context.watch(child)
  //I handle failures in the my children
  override val supervisorStrategy =
    OneForOneStrategy(maxNrOfRetries = 60, withinTimeRange = 1 minute) {
      case _: ResumeMakingException ⇒ Resume
      case _: RestartMakingException ⇒ Restart
      case _: EscalateMakingException ⇒ Restart
      case t: Throwable => Escalate // {System.err.println(s"Caught $t, responding with Restart signal"); Restart }
    }

  def receive = {
    case 'explode => child ! 'explode
    case 'shoot => child ! 'shoot
    case 'implode => child ! 'implode
    case 'armageddon => child ! 'armageddon
    case s: String => child ! s
    case o =>  System.err.println(s"unexpected message $o")
  }
}

//This is the top-level supervisor strategy
class FooGuardianSupervisorStrategyConfigurator extends SupervisorStrategyConfigurator {
  def create(): SupervisorStrategy = OneForOneStrategy(maxNrOfRetries = 60, withinTimeRange = 1 minute) {
    case _: ResumeMakingException ⇒ Resume
    case _: RestartMakingException ⇒ Restart
    case _: EscalateMakingException ⇒ Restart
    case t: Throwable => {System.err.println(s"Caught $t, responding with Restart signal"); Restart }
  }
}

object Booter {
  def main(args: Array[String]) {
    implicit val as = ActorSystem("wombles")


    //    akka.actor.guardian-supervisor-strategy, which takes the fully-
    //      qualified class-name of a SupervisorStrategyConfigurator. When the guardian escalates a failure, the
    //    root guardian’s response will be to terminate the guardian, which in effect will shut down the whole actor system.

    val f = as.actorOf(Props[Foo], "foo")
    f ! "hello before exploding"
    f ! 'explode
    f ! "hello after exploding"
    f ! "hello"
    f ! "hello before shooting"
    f ! 'shoot
    f ! "hello after shooting"
    f ! "hello"
    f ! 'implode
    f ! "hello after imploding"
    f ! "hello before armageddon"
    f ! 'armageddon
    f ! "hello after armageddon"
    f ! "hello"

    val c = new CountDownLatch(1)
    c.await(5, TimeUnit.SECONDS)

    f ! "hello ! Really, surely after armageddon"
    val c4 = new CountDownLatch(1)
    c4.await(10, TimeUnit.SECONDS)
    as.shutdown()
  }
}
