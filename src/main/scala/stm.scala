import scala.concurrent.stm._

object STMSampleMain {
  def main(args: Array[String]) {
    def setString {
      val last = Ref("none")
      atomic { implicit txn =>
        last() = "set by outer"
        try {
          atomic { implicit txn =>
            last() = "set by inner"
            throw new RuntimeException
          }
        } catch {
          case _: RuntimeException => println("Got runtime exception, rolled back the value")
        }
      }
      atomic {
        implicit txn =>
          println(last.get)
      }
    }
    def incrementInt {
      val last = Ref(1)
      atomic { implicit txn =>
        last() = last() + 1
        try {
          atomic { implicit txn =>
            //Never gets incremented here
            last() = last() + 1
            throw new RuntimeException
          }
        } catch {
          case _: RuntimeException => println("Got runtime exception, rolled back the value")
        }
      }
      atomic {
        implicit txn =>
          println( s"last should be = 2 , last = ${last.get}")
      }
    }

    setString
    incrementInt
  }
}
